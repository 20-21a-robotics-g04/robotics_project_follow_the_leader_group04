#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
import cv2
import time
import os.path
from os import path
from gopigo import *


# Open the camera
cap = cv2.VideoCapture(0)

if path.exists('trackbar_defaults.txt'):
    f= open("trackbar_defaults.txt","r")
    line=f.readlines()
    trackbar1_value = int(line[0])
    trackbar2_value = int(line[1])
    trackbar3_value = int(line[2])
    trackbar4_value = int(line[3])
    trackbar5_value = int(line[4])
    trackbar6_value = int(line[5])
else:
    trackbar1_value = 154
    trackbar2_value = 141
    trackbar3_value = 72
    trackbar4_value = 229
    trackbar5_value = 253
    trackbar6_value = 255


def updateValue1(new_value1):
    # Make sure to write the new value into the global variable
    global trackbar1_value
    trackbar1_value = new_value1

def updateValue2(new_value2):
    # Make sure to write the new value into the global variable
    global trackbar2_value
    trackbar2_value = new_value2

def updateValue3(new_value3):
    # Make sure to write the new value into the global variable
    global trackbar3_value
    trackbar3_value = new_value3

def updateValue4(new_value4):
    # Make sure to write the new value into the global variable
    global trackbar4_value
    trackbar4_value = new_value4

def updateValue5(new_value5):
    # Make sure to write the new value into the global variable
    global trackbar5_value
    trackbar5_value = new_value5

def updateValue6(new_value6):
    # Make sure to write the new value into the global variable
    global trackbar6_value
    trackbar6_value = new_value6
    

cv2.namedWindow("Original")

cv2.createTrackbar("Hl", "Original" , trackbar1_value, 255, updateValue1)
cv2.createTrackbar("Sl", "Original" , trackbar2_value, 255, updateValue2)
cv2.createTrackbar("Vl", "Original" , trackbar3_value, 255, updateValue3)

cv2.createTrackbar("Hh", "Original" , trackbar4_value, 255, updateValue4)
cv2.createTrackbar("Sh", "Original" , trackbar5_value, 255, updateValue5)
cv2.createTrackbar("Vh", "Original" , trackbar6_value, 255, updateValue6)


blobparams = cv2.SimpleBlobDetector_Params()
blobparams.filterByCircularity = False
blobparams.filterByInertia = False
blobparams.filterByConvexity = False
blobparams.filterByArea = True
blobparams.minArea = 900 #500 #700
blobparams.maxArea = 10000000
blobparams.filterByColor = True
blobparams.blobColor = 255
detector = cv2.SimpleBlobDetector_create(blobparams)
detectorbilat = cv2.SimpleBlobDetector_create(blobparams)

#x-coordinate list

X_lst = []
def X_lst_append(X):
    X_lst.append(X)
    if len(X_lst) > 501:
        X_lst.pop(0)


state = "finding"


while True:
    # Read the image from the camera
    ret, frame = cap.read()
    #print(frame.shape)

    # You will need this later
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    frame = frame[230:280] #change frame if needed
    [height,width,channels] = frame.shape[:]
    
    # Colour detection limits
    lH = trackbar1_value
    lS = trackbar2_value
    lV = trackbar3_value
    hH = trackbar4_value
    hS = trackbar5_value
    hV = trackbar6_value
    lowerLimits = np.array([lH, lS, lV])
    upperLimits = np.array([hH, hS, hV])

    # Our operations on the frame come here
    thresholded = cv2.inRange(frame, lowerLimits, upperLimits)
    thresholded = cv2.rectangle(thresholded, (0, 0), (frame.shape[1]-1, frame.shape[0]-1), (0, 0, 0), 10)


    #detect and draw keypoints
    keypoints = detector.detect(thresholded)
    original = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    print(len(keypoints))
    print(state)
    
    if state == "finding":
        set_speed(30)
        if len(X_lst) == 0 or (X_lst[-1] < width/2 - 60):
            left_rot()
        
        elif (X_lst[-1] > width/2 + 60):
            right_rot()
            
        else:
            stop()
           
    
        for keypoint in keypoints:
            cv2.putText(frame, str(int(keypoint.pt[0]))+","+str(int(keypoint.pt[1])),(int(keypoint.pt[0]), int(keypoint.pt[1])), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
            
        
        if len(keypoints) == 1:
            stop()
            state = "found"
            
            
        
    elif state == "found":
    #find center of object and write its coordinates
        for keypoint in keypoints:
            X = int(np.round(keypoint.pt[0]))
            X_lst_append(X)
            Y = int(np.round(keypoint.pt[1]))
            print(keypoint.size)
            
        
            if keypoint.size < 130:
                print('forward')
                motor_fwd()

                if (X < width/2 - 60):
                    set_right_speed(70)
                    set_left_speed(60)
                elif (X > width/2 + 60):
                    set_left_speed(70)
                    set_right_speed(60)
                elif (X > width/2 - 60) or (X < width/2 + 60):
                    set_right_speed(60)
                    set_left_speed(60)
                    fwd()

            
                            
            elif keypoint.size > 150:            
                motor_bwd()
                set_right_speed(50)
                set_left_speed(60)
                print('back')
            
            else:
                stop()
                print('stop')
        
            
        if len(keypoints) == 0:
            state = "finding"
        
        
        
    # Display the resulting frame
    cv2.imshow('Processed', thresholded)


    # Quit the program when 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        stop()
        break


f = open("trackbar_defaults.txt","w+")
f.write(str(trackbar1_value) + "\n" +
        str(trackbar2_value) + "\n" +
        str(trackbar3_value) + "\n" +
        str(trackbar4_value) + "\n" +
        str(trackbar5_value) + "\n" +
        str(trackbar6_value) + "\n")
f.close()

# When everything done, release the capture
print('closing program')
cap.release()
cv2.destroyAllWindows()
