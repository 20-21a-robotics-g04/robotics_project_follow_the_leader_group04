
# README #

This README contains the name of our project, project overview, schedule, components list and team members.

### Follow the Leader Group 04 members###

* Marilin Moor
* Valeriia Leoshko
* Uladzislava Kastsitsyna
* Gleb Kovalev

### Overview ###

There are two different robots: a leader robot and a follower robot. The leader robot detects the obstacle course. The follower robot should avoid obstacles but not by detecting the obstacles but just following the leader. 
The obstacle course will be designed by ourselves and approved by supervisor.\We assume that the leading robot should be the ClearBot robot designed by the University of Tartu research group. 
It has a RGB-D camera and is controlled by ROS. The robot-follower is a GoPiGo robot, controlled by a Raspberry Pi and connected to a Logitech USB camera for visual information. 
As the leading robot should navigate the following robot, we will use a monochrome material that is placed around the leading robot so that the robot-follower is able to detect the blob on the leader whatever the orientation of the leading robot. 
The robot-follower should recognise the monochrome label on the leading robot and follow it. It is also important to note that robots should not crash into each other or the obstacles. 
This could be achieved with the implementation of a distance sensor, for example, an ultrasonic sensor. 
On top of that, the following robot needs to be able to handle briefly losing sight of the leading robot.

Overall, our tasks could be divided into 2 categories:

1) The operation of the robot-follower GoPiGo, which includes the detection of the robot-leader and its control based on sensory feedback.

2) Collective tasks which include the operation of the leading Clearbot robot - obstacle detection and its control based on video feedback, and the setup of communication between 2 modules and implementation of the hardware solution with integrated software,

The tasks related to GoPiGo can be considered as individual tasks, and are distributed among the team members.

### Schedule ###

Based on insights into the robots' configuration, we have decided to deal with the already known GoPiGo robot first - initial configuration, blob detection and moving towards it, connecting the ultrasonic sensor (first connected to Arduino) to Pi and obtaining information from the pin, so that the following robot moves accordingly.

Secondly, as a collective we have decided to tackle the tasks related to ClearBot, as this is new hardware, documentation, uses a separate environment for coding - ROS, all-in-all, seems to be a lot more time-consuming.

This said, we have decided to schedule our project in the following way:

**November 4-11**: starting with individual tasks related to GoPiGo2(OS setup, WiFi configuration, robot detecting blobs and move accordingly, robot to understand the feedback from the ultrasonic sensor and move accordingly (stop if distance less than x cm), obstacle course (20h) 

**November 11-18**: validating the codes for working with GoPiGo2 (debugging), setting up environment for ClearBot, understanding the documentation and possibly testing some motion (20h) 

**November 18-25**: initial testing between two robots (one follows the other in a straight line), ClearBot movement through the obstacle course, demo video preparation (20h) 

**November 25 - December 2**: Hardware and software full implementation, ClearBot leading the GoPiGo through the obstacle course, troubleshooting (20h) 

**December 2-9**: final solution testing (10h) 

**December 9-16**: continue testing and poster preparation (30h) 

**December 16 - January**: presentation preparation (20h)




### Components list ###

| Item                                      | Link to the item                                                                                     | We will provide | Need from instructors | Total |
|-------------------------------------------|------------------------------------------------------------------------------------------------------|:---------------:|:---------------------:|:-----:|
| Clearbot                                  | https://clearbot.eu/et/tehnoloogia/                                                                  | 0               | 1                     | 1     |
| GoPiGo2 (with power connector)            | https://www.generationrobots.com/en/402293-gopigo2-base-kit.html                                     | 0               | 1                     | 1     |
| Logitech USB camera                       | https://www.logitech.com/en-us/products/webcams/c920-pro-hd-webcam.960-000764.html?crid=34           | 0               | 1                     | 1     |
| Raspberry Pi                              | https://www.raspberrypi.org/products/raspberry-pi-4-model-b/?resellerType=home                       | 0               | 1                     | 1     |
| Raspberry Pi power adapter                | https://www.oomipood.ee/product/3106255_toiteplokk_usb_c_5_1v_3a_euro_uk_raspberry_pi_4              | 0               | 1                     | 1     |
| Breadboard                                | https://www.sparkfun.com/products/12002                                                              | 0               | 1                     | 1     |
| LiPo battery alarm                        | https://www.amazon.co.uk/Battery-Tester-Voltage-Buzzer-Alarm/dp/B009LW48LK                           | 0               | 1                     | 1     |
| LiPo battery (3S 2200 mAh) (for GoPiGo 2) | https://hobbyking.com/ru_ru/turnigy-2200mah-3s-25c-lipo-pack.html?___store=ru_ru                     | 0               | 1                     | 1     |
| LiPo battery (4S 5200 mAh) (for Clearbot) | https://hobbyking.com/ru_ru/turnigy-high-capacity-5200mah-4s-12c-multi-rotor-lipo-pack-w-xt60.html   | 0               | 1                     | 1     |
| Micro SD card with an adapter             | https://onoff.ee/foto--ja-videokaamerad/malukaardid/samsung-evo-pro-microsdhc-32gb/                  | 0               | 1                     | 1     |
| Clearbot power adapter                    | https://clearbot.eu/et/tehnoloogia/                                                                  | 0               | 1                     | 1     |
| Ultrasonic sensor                         | https://www.sparkfun.com/products/15569                                                              | 0               | 1                     | 1     |
| Arduino Nano                              | https://store.arduino.cc/arduino-nano                                                                | 0               | 1                     | 1     |
| Jumper wires (FF, MM, FM)                 | https://www.sparkfun.com/products/9194                                                               | 0               | 3x 6                  | 18    |
| Obstacles to create the course            | https://www.kaubamaja.ee/polesie-keegel-h0338933, https://www.pakendikeskus.ee/kastid/pappkastid     | 2x 5 (min)      | 0                     | 10    |
| Monocolored material for blob detection   | https://www.abakhan.ee/ee/longad.html                                                                | min 1           | 0                     | 1     |
| Mini flashlight                           | https://www.prismamarket.ee/products/search/taskulamp%20                                             | 1               | 0                     | 1     |



### Challenges and Solutions ###

* Our progress: ClearBot is able to detect the obstacles by laser scanner and stop in front of them. If obstacles are removed it continues moving.
GoPiGo is following ClearBot by blob detection and is able to stop when ClearBot stops to avoid crushing. For the GoPiGo to stop we also used blob detection but later we are planning to use ultrasonic sensor as well. 

#### GoPiGo ####

* Ultrasonic sensor has not been implemented yet but we are going to add it in order to check if we are too close to ClearBot if blob detection does not work;

* Adding the ultrasonic sensor to the system: so far Uladizslava has tried to obtain legitimate values from the sensor, yet there seems to be an error in measuring the values;

* What has been done to remove the error: tested different wires, Arduino Nanos, ultrasonic sensors; debug the code as much as possible;

* Possible solution: debug the code more (Uladzislava);

* Improve GoPiGo movement code - sometimes not moving towards the blob once it has detected it (Marilin and Valeriia);

* Steering problem: sometimes when GoPiGo is spinning it gets stuck i.e. not continuously spinning;

* Solution: ask supervisor to check hardware (Gleb).

#### ClearBot ####

* Making ClearBot move around obstacles not just detect them;

* Possible solution: implement the logic control using laserscanning to measure distance, linear x and y and angular z velocity can be changed (whole team). 

#### Collective ####

* Creating the obstacle course: Valeriia and Gleb have dealt with fetching the components to create this course, the course will be about the robot driving on the road and what dangers (fallen trees,crossing deers)  it might face;

* Test the GoPiGo + ClearBot solution on the course, so that one won't crash into another and into the obstacles; 

* Task division: ClearBot - Gleb and Marilin, GoPiGo - Uladzislava and Valeriia.



