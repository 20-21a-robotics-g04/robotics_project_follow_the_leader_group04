#!/usr/bin/env python
import rospy
import time
from geometry_msgs.msg import Twist
from robotont_laserscan_to_distance.msg import LaserScanSplit

# Initiate an empty message
distances = LaserScanSplit()

#Set the distance
setpoint=0.65
setpoint1=0.4

# This is a subscriber callback
def scan_callback(data):
    global distances
    distances = data

# Stopping function
def stop_robot():
	global vel_msg, velocity_publisher
	vel_msg.linear.x = 0
	velocity_publisher.publish(vel_msg)

# Initialise the subscriber
rospy.Subscriber('/scan_to_distance', LaserScanSplit, scan_callback)

# Get distance to the nearest object in the center sector of the sensor's field of view
distance_to_nearest_object = distances.center_min

# Initialise a node where we can run our code
rospy.init_node('robotont_velocity_publisher', anonymous=True)

# Create a publisher for the driving command we will send to ensure they get to the motors
velocity_publisher = rospy.Publisher('/robotont/cmd_vel', Twist, queue_size=10)
# Initialise an empty message of type Twist
vel_msg = Twist()
# Initially set all speed components to 0 to ensure the robot does not drive unexpectedly
vel_msg.linear.x = 0
vel_msg.linear.y = 0
vel_msg.linear.z = 0
vel_msg.angular.x = 0
vel_msg.angular.y = 0
vel_msg.angular.z = 0

#counters
counter = 0
counter_right = 0
counter_left = 0

#state to count 
Right = False
Left = False

while not rospy.is_shutdown() and counter < 600:
	center = distances.center_min
	left = distances.left_min
	right = distances.right_min
	# Limit control freqnecy to 10 Hz.
	# This will be useful if printing things (makes output easier to read) or
	# controlling the robot (gives the robot time to execute previous commands)
	time.sleep(0.1)
	print("Left:", round(left, 2), "Right:", round(right,2), "Center", round(center,2), "SetPoint", round(setpoint,2))
	if center < setpoint:
		vel_msg.linear.x = 0.07
		print('I see the obstacle!')
		#left
		if right < setpoint and right < left:
			vel_msg.linear.x = 0.0
			vel_msg.linear.y = 0.08
			velocity_publisher.publish(vel_msg)
			if Right == "False":
				counter_left += 1
				Right = "True"
			Left = "False"


		#right
		elif left < setpoint and left < right:
			vel_msg.linear.x = 0.0
			vel_msg.linear.y = -0.08
			velocity_publisher.publish(vel_msg)
			if Left == "False":
				counter_right += 1
				Left = "True"
			Right = "False"

		#dead-end
		if (counter_right > 4) and (counter_left > 4):
			print("BACK!!!")

			vel_msg.linear.x = -0.2
			vel_msg.linear.y = 0.0
			velocity_publisher.publish(vel_msg)
			time.sleep(0.1)
			velocity_publisher.publish(vel_msg)
			time.sleep(3)

			if right < setpoint and right < left:
				vel_msg.linear.x = 0.0
				vel_msg.linear.y = 0.0
				vel_msg.angular.z = 0.9
				print("LEFT!!!")
				for i in range(2):
					velocity_publisher.publish(vel_msg)
					time.sleep(2)

			elif left < setpoint and left < right:
				vel_msg.linear.x = 0.0
				vel_msg.linear.y = 0.0
				vel_msg.angular.z = -0.9
				print("RIGHT!!!")
				for i in range(2):
					velocity_publisher.publish(vel_msg)
					time.sleep(2)

			vel_msg.angular.z = 0.0
			counter_right = 0
			counter_left = 0
 
		print("Left and Right counters:" , str(counter_left), str(counter_right))
		velocity_publisher.publish(vel_msg)
		 

	else:
		print("Everything is ok!")
		vel_msg.linear.x = 0.07
		vel_msg.linear.y = 0.0


		if right < setpoint1:

			vel_msg.linear.x = 0.0
			vel_msg.linear.y = 0.08

		elif left < setpoint1:

			vel_msg.linear.x = 0.0
			vel_msg.linear.y = -0.08

		velocity_publisher.publish(vel_msg)

	counter += 1

	# Send a stopping command to the robot
	rospy.on_shutdown(stop_robot)

	
