// Ultrasonic sensor
int echo_pin = A4;
int trig_pin = A5;

// Anything over 23200 us pulse (4000 mm) is "out of range"
const unsigned int ULTRASONIC_ECHO_TIMEOUT = 23200;

const int ULTRASONIC_DELAY = 10;
int us = 4000; // no reading, out of range value

long duration_us = 0;
long distance_in_mm = 0;

void setup()
{
    Serial.begin(115200);
    // Set pin directions for the ultrasonic sensor
    pinMode(echo_pin, INPUT);
    pinMode(trig_pin, OUTPUT);

}

void loop()
{
    // Get distance from wall with ultrasonic sensor
    us = get_US();

    // Read everything from serial
    if(Serial.available())
    {
        Serial.read();

        print_JSON(us); // Print data to serial.
    }

}

// Gets distance in mm from the ultrasonic sensor
long get_US()
{
    // TASK: get distance with ultrasonic, read about pulseIn() arguments and figure out how to use ULTRASONIC_ECHO_TIMEOUT
    //int distance_in_mm = 0;
    digitalWrite(trig_pin, HIGH);
    delayMicroseconds(ULTRASONIC_DELAY);
    digitalWrite(trig_pin, LOW);
    
    // Read the pulse HIGH state on echo_pin
    // the length of the pulse in microseconds
    duration_us = pulseIn(echo_pin, HIGH, ULTRASONIC_ECHO_TIMEOUT);
 
    distance_in_mm = (duration_us * 0.34)/2 ;

    return distance_in_mm;
}

// Print all the sensor data to serial as JSON
void print_JSON(int us)
{ 
    Serial.print("{\"us\":");
    Serial.print(us);
    Serial.println("}");
}
